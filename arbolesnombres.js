class Node {
  constructor (value) {
    this.value = value
    this.right = null
    this.left = null
  }
}
class Tree {
  constructor () {
    this.root = null
  }

  isEmpty () {
    return this.root === null
  }

  add (value) {
      if (this.isEmpty()) {
          this.root = new Node(value)
          return
      }
      var aux = this.root
      while (aux) {
          if (value < aux.value) {
          if (aux.right) {
              aux = aux.right
          } else {
              aux.right = new Node(value)
              return
          }
          } else {
          if (aux.left) {
              aux = aux.left
          } else {
              aux.left = new Node(value)
              return
          }
        }
      }
  }
  findMin(node = this.root) {
      if (!this.isEmpty()) {
        while (node.left) {
          node = node.left
        }
        return node
      }
  }
  delete (value, node = this.root) {
    if (!node) {
      return null
    }
    if (node.value === value) {
      if (!node.left && !node.right) {
        return null
      }
      if (!node.left) {
        return node.right
      }
      if (!node.right) {
        return node.left
      }
  var temp = this.findMin(node.right)
      node.value = temp.value;
      node.right = this.delete(temp.value, node.right)
      return node;
    }
    if (node.value < value) {
      node.right = this.delete(value, node.right)
      return node
    }
    if (node.value > value) {
      node.left = this.delete(value, node.left)
      return node
    }
  }
  print (node = this.root) {
    if (!node) {
      return
    }
    this.print(node.left)
    console.log(node.value)
    this.print(node.right)
  }
  printr (node = this.root) {
      if (!node) {
        return
      }
      console.log(node.value)
      this.print(node.right)
  }
  printl (node = this.root) {
      if (!node) {
        return
      }
      console.log(node.value)
      this.print(node.left)
}
}
var tex = new Tree()
var arr = []
tex.add(6.9+" minimo nesesario")
tex.add(9+" carlkos")
tex.add(9+" manuel")
tex.add(5+" ignacio")
tex.add(7+" guillermo")
tex.add(8+" anggel")
tex.add(9+" anselmo")
tex.add(4+" belen")
tex.add(9+" arturo")
tex.add(9+" teseo")
tex.add(6+" briana")
tex.add(5+" tamara")
tex.add(0+" braulio")
tex.add(5+" emanuel")
tex.add(8+" elena")
tex.add(9+" ursula")
tex.add(6+" esmeralda")
tex.add(5+" queila")
tex.add(7+" quike")
tex.add(9+" queta")
tex.add(6+" urian")
console.log("adicion en orden descendente")
tex.print()
console.log("alubmos reprobados")
tex.printr()

