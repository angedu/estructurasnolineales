class Node {
    constructor (value) {
      this.value = value
      this.right = null
      this.left = null
    }
  }
  
  class Tree {
    constructor () {
      this.root = null
    }
  
    isEmpty () {
      return this.root === null
    }
  
    add (value) {
        if (this.isEmpty()) {
            this.root = new Node(value)
            return
        }
        var aux = this.root
        while (aux) {
            if (value < aux.value) {
            if (aux.left) {
                aux = aux.left
            } else {
                aux.left = new Node(value)
                return
            }
            } else {
            if (aux.right) {
                aux = aux.right
            } else {
                aux.right = new Node(value)
                return
            }
            }
        }
    }
    
  addI (value) {
    if (this.isEmpty()) {
        this.root = new Node(value)
        return
    }
    var aux = this.root
    while (aux) {
        if (value < aux.value) {
        if (aux.right) {
            aux = aux.right
        } else {
            aux.right = new Node(value)
            return
        }
        } else {
        if (aux.left) {
            aux = aux.left
        } else {
            aux.left = new Node(value)
            return
        }
      }
    }
}
    findMin(node = this.root) {
        if (!this.isEmpty()) {
          while (node.left) {
            node = node.left
          }
          return node
        }
    }
    delete (value, node = this.root) {
      if (!node) {
        return null
      }
      if (node.value === value) {
        if (!node.left && !node.right) {
          return null
        }
        if (!node.left) {
          return node.right
        }
        if (!node.right) {
          return node.left
        }
        var temp = this.findMin(node.right)
        node.value = temp.value;
        node.right = this.delete(temp.value, node.right)
        return node;
      }
      if (node.value < value) {
        node.right = this.delete(value, node.right)
        return node
      }
      if (node.value > value) {
        node.left = this.delete(value, node.left)
        return node
      }
    }
    print (node = this.root) {
      if (!node) {
        return
      }
      this.print(node.left)
      console.log(node.value)
      this.print(node.right)
    }
    printr (node = this.root) {
        if (!node) {
          return
        }
        console.log(node.value)
        this.print(node.right)
    }
    printl (node = this.root) {
        if (!node) {
          return
        }
        this.print(node.left)
        console.log(node.value)
        }
}
  var Nordenados = new Tree()
  var arr = []
  Nordenados.add(0)
  Nordenados.add(1)
  Nordenados.add(5)
  Nordenados.add(-4)
  Nordenados.add(3)
  Nordenados.add(-3)
  Nordenados.add(-6)
  Nordenados.add(2)
  Nordenados.add(7)
  Nordenados.add(-5)
  Nordenados.add(-2)
  Nordenados.add(-1)
  Nordenados.add(12)
  console.log("numeros ordenados")
  Nordenados.print()
  console.log("numeros negativos")
  Nordenados.printl()
  console.log("numeros pocitibos")
  Nordenados.printr()

  var NumI = new Tree()
  var arr = []
  NumI.addI(0)
  NumI.addI(1)
  NumI.addI(-3)
  NumI.addI(-4)
  NumI.addI(-8)
  NumI.addI(-9)
  NumI.addI(-22)
  NumI.addI(-31)
  NumI.addI(-15)
  NumI.addI(-5)
  NumI.addI(4)
  NumI.addI(6)
  NumI.addI(8)
  NumI.addI(2)
  NumI.addI(42)
  NumI.addI(13)
  NumI.addI(11)
  NumI.addI(10)
  NumI.addI(9)
  NumI.addI(15)
  console.log("addicion imbersa")
  NumI.print()